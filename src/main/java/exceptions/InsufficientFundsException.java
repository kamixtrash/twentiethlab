package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда сумма в переводе
 * больше той, что имеется на карте.
 */
public class InsufficientFundsException extends RuntimeException {

    public InsufficientFundsException() {
    }

    public InsufficientFundsException(String message) {
        super(message);
    }
}

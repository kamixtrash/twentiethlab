package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда заблокированный
 * пользователь пытается выполнить какое-либо действие.
 */
public class AccountIsLockedException extends RuntimeException {
    public AccountIsLockedException() {
    }

    public AccountIsLockedException(String message) {
        super(message);
    }
}

package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда сумма
 * перевода больше определённого числа.
 */
public class SuspiciousAmountException extends RuntimeException {
    public SuspiciousAmountException() {
    }

    public SuspiciousAmountException(String message) {
        super(message);
    }
}

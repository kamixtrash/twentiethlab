package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда пользователь
 * вводит неправильный пин-код.
 */
public class InvalidPinException extends RuntimeException {
    public InvalidPinException() {
    }

    public InvalidPinException(String message) {
        super(message);
    }
}

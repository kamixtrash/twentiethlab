package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда сумма
 * перевода не выполняет каких-либо условий.
 */
public class WrongAmountException extends RuntimeException {
    public WrongAmountException() {
    }

    public WrongAmountException(String message) {
        super(message);
    }
}

package model.transaction;

import model.client.Client;
import model.currency.Currency;

/**
 * Перевод с счёта на счёт другого пользователя.
 */
public class AccountToAccountTransaction extends Transaction {
    private static final boolean isWithdraw = false;

    /**
     * @param amount          сумма перевода.
     * @param currency        валюта, в которой производится перевод.
     * @param recipient       получатель.
     * @param sender          отправитель.
     */
    public AccountToAccountTransaction(Long amount, Currency currency, Client recipient, Client sender) {
        super(amount, currency, recipient, sender, isWithdraw);
    }

    @Override
    public String toString() {
        return "Account to account transaction: " + super.toString();
    }
}
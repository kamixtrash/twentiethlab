package model.transaction;

import model.client.Client;
import model.currency.Currency;

/**
 * Перевод с карты на карту другого пользователя.
 */
public class CardToCardTransaction extends Transaction {
    private static final boolean isWithdraw = false;

    /**
     * @param amount          сумма перевода.
     * @param currency        валюта, в которой производится перевод.
     * @param recipient       получатель.
     * @param sender          отправитель.
     */
    public CardToCardTransaction(Long amount, Currency currency, Client recipient, Client sender) {
        super(amount, currency, recipient, sender, isWithdraw);
    }

    @Override
    public String toString() {
        return "Card to card transaction: " + super.toString();
    }
}
package model.terminal;

import exceptions.InsufficientFundsException;
import exceptions.WrongAmountException;
import model.client.Client;
import model.currency.Currency;
import model.transaction.AccountToAccountTransaction;
import model.transaction.CardToCardTransaction;
import model.transaction.Transaction;
import model.transaction.WithdrawTransaction;

import java.util.ArrayList;

public class TerminalServer {
    private final ArrayList<Transaction> transactions;

    public TerminalServer() {
        this.transactions = new ArrayList<>();
    }

    public Transaction createTransaction(Long amount, Currency currency, Client recipient, Client sender, Integer type) {
        switch (type) {
            case 1:
                transactions.add(new AccountToAccountTransaction(amount, currency, recipient, sender));
                break;
            case 2:
                transactions.add(new CardToCardTransaction(amount, currency, recipient, sender));
                break;
            case 3:
                transactions.add(new WithdrawTransaction(amount, currency, sender));
                break;
            default:
                return null;
        }
        return transactions.get(transactions.size()-1);
    }

    public boolean commitTransaction(Transaction transaction) throws InsufficientFundsException, WrongAmountException {
        return transaction.commit();
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }
}

package client;

import model.client.Client;
import model.client.Sender;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

public class ClientToStringTests {

    Client testClient;

    @BeforeMethod
    public void prepareClient() {
        testClient = new Sender("Ivanov", "Ivan", "Ivanovich", null, null, null, null);
    }

    @Test
    public void normalToString() {
        assertEquals("Ivanov Ivan Ivanovich", testClient.toString());
    }

    @Test
    public void lowerToString() {
        assertEquals("ivanov ivan ivanovich", testClient.toStringLower());
    }

    @Test
    public void upperToString() {
        assertEquals("IVANOV IVAN IVANOVICH", testClient.toStringUpper());
    }
}

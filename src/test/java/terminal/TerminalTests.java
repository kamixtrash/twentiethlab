package terminal;

import exceptions.AccountIsLockedException;
import exceptions.InvalidPinException;
import exceptions.WrongAmountException;
import model.client.Client;
import model.client.Sender;
import model.currency.Currency;
import model.terminal.*;
import model.transaction.AccountToAccountTransaction;
import model.transaction.Transaction;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.testng.AssertJUnit.*;

public class TerminalTests {

    @Mock
    TerminalServer terminalServer;
    @Mock
    PinValidator pinValidator;
    @Mock
    FrodMonitor frodMonitor;
    @Mock
    Client sender;
    @Mock
    Client recipient;

    Transaction rightTransaction = new AccountToAccountTransaction(100L, Currency.RUB, recipient, sender);

    public TerminalTests() {
        MockitoAnnotations.openMocks(this);
    }

    @BeforeMethod
    public void prepareData() {
        when(sender.getPin()).thenReturn("1234");
        when(sender.getBalance()).thenReturn(1000L);
    }

    @Test
    public void createTransactionTest_Success() {
        System.out.println("Started successful transaction test.");
        TerminalServer terminalServer = new TerminalServer();
        Terminal testTerminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
        doNothing().when(pinValidator).validate(anyString(), eq(sender));
        when(frodMonitor.checkTransaction(any(Transaction.class))).thenReturn(true);
        Transaction testTransaction = testTerminal.createTransaction(100L, Currency.RUB, recipient, sender, 1, "1234");
        System.out.println("Finished successful transaction test.");
    }

    @Test
    public void createTransactionTest_InvalidPin() {
        System.out.println("Started invalid pin transaction test.");
        Terminal testTerminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
        when(frodMonitor.checkTransaction(any(Transaction.class))).thenReturn(true);
        doThrow(new InvalidPinException()).when(pinValidator).validate("1233", sender);
        Transaction testTransaction = testTerminal.createTransaction(100L, Currency.RUB, recipient, sender, 1, "1233");
        System.out.println("Finished invalid pin transaction test.");
    }

    @Test
    public void createTransactionTest_AccountIsLocked() {
        System.out.println("Started locked account transaction test.");
        Terminal testTerminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
        when(frodMonitor.checkTransaction(any(Transaction.class))).thenReturn(true);
        doThrow(new AccountIsLockedException()).when(pinValidator).validate("1233", sender);
        Transaction testTransaction = testTerminal.createTransaction(100L, Currency.RUB, recipient, sender, 1, "1233");
        System.out.println("Finished locked account transaction test.");
    }

    @Test
    public void createTransactionTest_WrongAmount() {
        System.out.println("Started transaction test with wrong amount of money.");
        TerminalServer terminalServer = new TerminalServer();
        FrodMonitor frodMonitor = new FrodMonitor(terminalServer);
        Terminal testTerminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
        doNothing().when(pinValidator).validate("1234", sender);
        Transaction testTransaction = testTerminal.createTransaction(125L, Currency.RUB, recipient, sender, 1, "1234");
        try {
            testTransaction.check();
        } catch (WrongAmountException e) {
            System.out.println("Detected wrong amount in transaction.");
        }
        System.out.println("Finished transaction test with wrong amount of money.");
    }
}
